library(testthat)
source('functions.R')

# R -e "library(testthat); test_dir('.', filter='unit')"
testthat::context('LoadTreeData')

test_that('LoadTreeData OK', {
  file <- 'arbres-publics.csv'
  expect_true(file.exists(file))

  minDia <- 322.99

  res <- LoadTreeData(file, minDiameter = minDia)

  expect_equal(nrow(res), 8)

  obsMinDia <- min(res$DHP, na.rm = T)

  expect_true(obsMinDia >= minDia)
  expect_equal(obsMinDia, 323, tolerance = 1e-2, scale = 1)
})
