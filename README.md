<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Arbres Publics Montréal](#arbres-publics-montréal)
	- [Technologies](#technologies)
	- [Screenshot](#screenshot)
	- [Live](#live)
	- [Credits](#credits)

<!-- /TOC -->

# Arbres Publics Montréal

This is a **Web Application** displaying [open data](http://donnees.ville.montreal.qc.ca/dataset/arbres)
about public trees in Montréal (Canada).

## Technologies

It is built using:
* [R](https://www.r-project.org/)
* [Shiny](https://shiny.rstudio.com/)
* [Leaflet](https://rstudio.github.io/leaflet/shiny.html)
* [DT](https://rstudio.github.io/DT/)

## Screenshot

It looks like this:

![](main-page-1.png)

## Live

It is available at:

https://apoirier.shinyapps.io/arbres-publics-montreal/

## Credits

Icon <img src="www/tree.png" height=40/> made by [Pixel Buddha](https://www.flaticon.com/authors/pixel-buddha)
from [www.flaticon.com](https://www.flaticon.com/).
